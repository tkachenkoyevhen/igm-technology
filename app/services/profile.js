const Profile = {
    name: 'profile',
    store: [],

    addUser(user) {
        const {fname, lname, phone, address} = JSON.parse(Object.keys(user));
        const id = this.generateID();
        this.store.push({
            id: id,
            fname: fname,
            lname: lname,
            phone: phone,
            address: address
        });
    },

    removeUser(id) {
        this.store = this.store.filter((el) => el.id != JSON.parse(Object.keys(id)));
        return this.store;
    },

    removeAllUsers() {
        this.store.length=0;
    },

    updateUser(user) {
        const { id } = JSON.parse(Object.keys(user));
        let index = this.store.findIndex((el) => el.id == id);
        this.store[index] = {...JSON.parse(Object.keys(user))}
        return this.store;
    },

    getUser(id) {},

    getAllUser() {
        return this.store;
    },

    generateID() {
        return (!this.store.length) ? 1 : (this.store.sort((a, b) => b.id - a.id)[0].id + 1)
    }
}

module.exports = Profile;