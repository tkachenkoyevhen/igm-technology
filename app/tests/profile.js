const profile = require('../services/profile');
const app = require('../index').app;
const host = 'http://localhost:8080';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const should = chai.should();

chai.use(chaiHttp);

describe('Profile', () => {
    it('Before test - remove all users.', (done) => {
        profile.removeAllUsers();
        done();
    })

    it('It is a test of functionality - get all users.', (done) => {
        chai.request(host)
        .get('/')
        .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.deep.equal({});

            done();
        })
    });

    it('It is a test of functionality - add user.', (done) => {
        const user = {
            id: 1,
            fname: 'John',
            lname: 'Smith',
            phone: '777777777',
            address: 'john@email.ca'
        };

        chai.request(host)
        .post('/add')
        .send(JSON.stringify(user))
        .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body.data).to.not.equal({});
            expect(res.body.data).to.be.an('array').to.deep.include(user);
            done();
        });
    });

    it('It is a test of change user parameters.', (done) => {
        const user = {
            id: 1,
            fname: 'Ted',
            lname: 'Potma',
            phone: '999999999',
            address: 'ted@mail.ca'
        };

        chai.request(host)
        .put('/update')
        .send(JSON.stringify(user))
        .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body.data).to.not.equal({});
            expect(res.body.data).to.deep.include(user);
            done();
        });
    })

    it('It is a test which remove user from store.', (done) => {
        chai.request(host)
        .delete('/del')
        .send('1')
        .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body.data).to.not.equal([]);
            done();
        });
    })
});