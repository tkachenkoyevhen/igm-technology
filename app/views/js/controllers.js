function ListCtrl($scope, $http) {
    const hostname = 'http://localhost:8080';
    const headerParams = { 'Content-Type': 'application/x-www-form-urlencoded' };
    $scope.users = users;
    $scope.btn = 'add';

    function clearModel() {
        $scope.user.fname='';
        $scope.user.lname='';
        $scope.user.phone='';
        $scope.user.address='';
        $scope.btn = 'add';
    }

    // The function wrapper for http request
    function httpRequest(method, action, data=null, headers=null) {
        return $http({
            method  : method,
            url     :`${hostname}/${action}`,
            data    : data,
            headers : headers
        });
    }

    // Submit function.
    $scope.submit = () => {
        let action = ($scope.btn == 'add') ? 'POST' : 'PUT';
        httpRequest(action, $scope.btn, $scope.user, headerParams)
        .then(
            (response) => {
                $scope.users = response.data['data'];
                clearModel();
            }
        ).catch(
            (err) => {
                console.log(err)
            }
        );
    }

    // Update is make preset to form
    $scope.update = (id) => {
        const { fname, lname, phone, address}  = $scope.users.find((el) => el.id == id);
        $scope.btn = 'update';
        $scope.user.id = id;
        $scope.user.fname = fname;
        $scope.user.lname = lname;
        $scope.user.phone = phone;
        $scope.user.address = address;
    }

    // Remove action
    $scope.remove = (id) => {
        httpRequest('DELETE', 'del', id, headerParams)
        .then(
            (response) => {
                $scope.users = response.data['data']
            }
        ).catch(
            (err) => {
                console.log(err)
            }
        )
    }
}

profileApp.controller('ListCtrl', ['$scope', '$http', ListCtrl]);