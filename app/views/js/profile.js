const profileApp = angular.module('profileApp', ['ngMaterial', 'ngRoute', 'ui.mask']);

profileApp.config([
    '$routeProvider', '$locationProvider', 'uiMask.ConfigProvider',
    function config($routeProvider, $locationProvider, uiMaskConfigProvider) {
        $routeProvider.when(
            '/',
            {
                templateUrl: '../tmpl/list.html',
                controller: ListCtrl
            }
        );

        $locationProvider.html5Mode(true);

        uiMaskConfigProvider.maskDefinitions({'A': /[a-z]/, '*': /[a-zA-Z0-9]/});
        uiMaskConfigProvider.clearOnBlur(false);
        uiMaskConfigProvider.eventsToHandle(['input', 'keyup', 'click']);
    }
]);