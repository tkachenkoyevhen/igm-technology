const express = require('express'),
		path = require('path'),
		app = express();
const bodyParser = require('body-parser');

const title = 'Contacts';
const profile = require('../app/services/profile.js');

app.use(express.static(path.join(__dirname + '/../node_modules')));
app.use(express.static(path.join(__dirname + '/views')));
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	res.render(
		'index.jade',
		{
			title: title,
			data: profile.getAllUser(),
			msg: ''
		}
	);
});

app.post('/add', (req, res) => {
	profile.addUser(req.body)
	res.send(
		{
			title: title,
			data: profile.getAllUser(),
			msg: ''
		}
	);
});

app.delete('/del', (req, res) => {
	const items = profile.removeUser(req.body);

	res.send(
		{
			title: title,
			data: items,
			msg: ''
		}
	);
});

app.put('/update', (req, res) => {
	const items = profile.updateUser(req.body);

	res.send(
		{
			title: title,
			data: items,
			msg: ''
		}
	)
})

app.all('*', (req, res) => {
	res.send({
		title: title,
		msg: '404'
	});
});

app.listen(8080, () => {
	console.log('Listening on port 8080');
});

module.exports = app;